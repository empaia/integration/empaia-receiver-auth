# vNext

* allow disable audience check
* added setting to rewrite URL in wellknown
* implemented issuer verification
* added explicit audience error message
* separate handling for decode error vs. other invalid token error
* set request timeout to prevent indefinitely hang